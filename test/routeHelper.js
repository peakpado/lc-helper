/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test routeHelper.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var assert = require('assert');
var routeHelper = require('../').routeHelper;


describe('routeHelper methods', function () {

  it('createError should create eror with statusCode', function (done) {

    var err = routeHelper.createError('error message', 503);
    assert.equal(err.message, 'error message');
    assert.equal(err.statusCode, 503);
    done();
  });

  it('getRefIdField should return reference Id field', function (done) {
    var refId = routeHelper.getRefIdField({name: 'Challenge'});
    assert.equal(refId, 'challengeId');
    done();
  });

});
