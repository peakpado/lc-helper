/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Helper methods for partial response handling.
 *
 * @version 1.0
 * @author lovefreya
 */
'use strict';

var _ = require('lodash');
var async = require('async');
var inflection = require('inflection');
var errors = require('common-errors');

var dataSource;

/**
 * Judge whether the input character is accepted or not.
 * @param char
 * @returns {boolean}
 */
function _allowedCharacter(char) {
  var allowedCharacterList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  allowedCharacterList += 'abcdefghijklmnopqrstuvwxyz';
  allowedCharacterList += '_,()';
  for (var i = 0; i < allowedCharacterList.length; i += 1) {
    if (char === allowedCharacterList[i]) {
      return true;
    }
  }
  return false;
}

/**
 * Return offset.
 * @param string
 * @throws {Error}
 */
function _findRightBracket(string) {
  var count = 0;
  for (var i = 0; i < string.length; i += 1) {
    if (string[i] === ')') {
      if (i === 0) {
        throw new errors.ValidationError('Fields parameter cannot take empty object ().');
      }
      if (count === 0) {
        return i + 1;
      } else {
        count -= 1;
      }
    }
    if (string[i] === '(') {
      count += 1;
    }
  }
  throw new errors.ValidationError('Fields parameter must take entire pair of \'()\' .');
}

/**
 *
 * @param param string waiting to parse
 * @param entity the parsed object will append to this entity's key
 * @param property the key in entity
 * @throws ValidationError
 */
function iterationParse(param, entity, property) {
  var subObject = null;
  if (!property) {
    subObject = entity;
  } else {
    subObject = entity[property];
  }
  var cache = '';
  for (var i = 0; i < param.length; i += 1) {
    if (i === (param.length - 1)) {
      if (_allowedCharacter(param[i]) && param[i]!=='(' && param[i]!==',') {
        cache += param[i];
        subObject[cache] = true;
        cache = '';
      } else {
        throw new errors.ValidationError('Fields parameter cannot end up with a \'' + param[i] + '\' .');
      }
      return;
    } else if (param[i] === ',') {
      if (i === 0) {
        throw new errors.ValidationError('Fields parameter cannot start with a \',\' .');
      }
      subObject[cache] = true;
      cache = '';
    } else if (param[i] === '(') {
      var rightPos = i + _findRightBracket(param.substring(i + 1, param.length));
      //Now cache is a plural for the name of a known model.
      subObject[cache] = {};
      iterationParse(param.substring(i + 1, rightPos), subObject, cache);
      cache = '';
      i = rightPos + 1;
      if (param[i]) {
        if (param[i] === ')') {
          throw new errors.ValidationError('Fields parameter must take entire pair of \'()\' .');
        } else if (!_allowedCharacter(param[i])) {
          throw new errors.ValidationError('Fields parameter cannot contain character \'' + param[i] + '\' .');
        } else if (param[i] !== ',') {
          throw new errors.ValidationError('Fields parameter format error.');
        }
      }
    } else {
      if (_allowedCharacter(param[i])) {
        cache += param[i];
      } else {
        throw new errors.ValidationError('Fields parameter cannot contain character \'' + param[i] + '\' .');
      }
    }
  }
}

/**
 * If Model has this key, return true. Otherwise false.
 * @param Model
 * @param key
 */
function _hasKey(Model, key) {
  var has = false;
  _.forEach(_.keys(Model.rawAttributes), function (column) {
    if (key === column) {
      has = true;
    }
  });
  return has;
}

/**
 * If Model has many models, return true. Otherwise false.
 * Support caml-case item: Model Scorecard, models: scorecardItems
 * @param Model
 * @param models
 */
function _hasMany(Model, models) {
  var has = false;
  _.forEach(Model.associations, function (association) {
    if (((association.as === models) || (models === inflection.camelize(association.as, true))) &&
      (association.associationType === 'HasMany')) {
        has = true;
    }
  });
  return has;
}

/**
 * IF Model has this foreign key, return reference Model Name and foreignKey. Otherwise false.
 * This depends on the the sequelize model definition.
 * Model.belongsTo(FatherModel, {foreignKey: key})
 * @param Model
 * @param key
 */
function _hasForeignKey(Model, key) {
  var has = null;
  _.forEach(Model.associations, function (association) {
    if( (association.identifier===key+'Id' ||
          association.identifier===inflection.singularize(key)+'Id') &&
        association.associationType === 'BelongsTo'){
          has = [];
          has[0] = association.target.name;
          has[1] = association.identifier;
        }
  });
  return has;
}

/**
 *
 * @param Model Model need to be reduced
 * @param Entity the response entity wrapper
 * @param Property the key
 * @param Fields fields won't be reduced
 * @param callback
 */
function recursionReduce(Model, Entity, Property, Fields, callback) {
  var subObject;
  if (Property) {
    subObject = Entity[Property];
  } else {
    subObject = Entity;
  }

  if (_.isArray(subObject)) {
    var tasks = [];
    var index = -1;
    _.forEach(subObject, function (entity) {
      tasks.push(function (cb1){
        var reducedObject = {};
        var tasksB = [];
        if (!_.isObject(Fields)) {
          if (!subObject) {
            return cb1(null);
          }
          reducedObject = entity.values;
        } else {
          _.forEach(_.keys(Fields), function (key) {
            tasksB.push(function (cb2) {
              if (_hasKey(Model, key)) {
                reducedObject[key] = entity.values[key];
                cb2(null);
              } else if (_hasMany(Model, key)) {
                var modelName = inflection.capitalize(inflection.singularize(key));
                if (!dataSource[modelName]) {
                  modelName = inflection.camelize( inflection.underscore(key) ).slice(0, -1);
                }

                var foreignKey = Model.name.toLowerCase() + 'Id';
                var filter = {};
                filter[foreignKey] = entity.id;
                dataSource[modelName].findAll({where: filter}).success(function (entities) {
                  reducedObject[key] = entities;
                  recursionReduce(dataSource[modelName], reducedObject, key, Fields[key], cb2);
                }).error(function (err) {
                  reducedObject[key] = [];
                  cb2(new errors.Error('DBReadError: '+err.message, err));
                });
              } else {
                var reference = _hasForeignKey(Model, key);
                if (!reference) {
                  cb2(new errors.ValidationError(Model.name+' doesn\'t has ' + key));
                } else {
                  var filterOne = {};
                  filterOne.id = entity[reference[1]];
                  dataSource[reference[0]].find({where: filterOne}).success(function (entity) {
                    reducedObject[key] = entity;
                    recursionReduce(dataSource[reference[0]], reducedObject, key, Fields[key], cb2);
                  }).error(function (err) {
                    cb2(new errors.Error('DBReadError: '+err.message, err));
                  });
                }
              }
            });   // tasksB.push
          }); // forEach
        } // else

        index += 1;
        subObject[index] = reducedObject;
        async.series(tasksB, function (err) {
          cb1(err);
        });
      }); // tasks.push
    }); //forEach

    async.series(tasks, function (err) {
      callback(err, subObject);
    });
  } else {
    var reducedObject = {};
    var tasksC = [];

    if (!_.isObject(Fields)) {
      if (!subObject) {
        return callback(null);
      }
      reducedObject = subObject.values;
    } else {
      _.forEach(_.keys(Fields), function (key) {
        tasksC.push(function (cb3) {
          if (_hasKey(Model, key)) {
            reducedObject[key] = subObject.values[key];
            cb3(null);
          } else if (_hasMany(Model, key)) {
            var modelName = inflection.capitalize(inflection.singularize(key));
            if(!dataSource[modelName]){
              modelName = inflection.camelize( inflection.underscore(key) ).slice(0, -1);
            }

            var foreignKey = Model.name.toLowerCase() + 'Id';
            var filter = {};
            filter[foreignKey] = subObject.id;
            dataSource[modelName].findAll({where: filter}).success(function (entities) {
              reducedObject[key] = entities;
              recursionReduce(dataSource[modelName], reducedObject, key, Fields[key], cb3);
            }).error(function (err){
              reducedObject[key] = [];
              cb3(new errors.Error('DBReadError: '+err.message, err));  // 500 by default
            });
          } else {
            var reference = _hasForeignKey(Model, key);
            if (!reference) {
              cb3(new errors.ValidationError(Model.name+' doesn\'t has ' + key));
            } else {
              var filterOne = {};
              filterOne.id = subObject[reference[1]];
              dataSource[reference[0]].find({where: filterOne}).success(function(entity) {
                reducedObject[key] = entity;
                recursionReduce(dataSource[reference[0]], reducedObject, key, Fields[key], cb3);
              }).error(function (err) {
                cb3(new errors.Error('DBReadError: '+err.message, err));  // 500 by default
              });
            }
          }
        });  // taskC.push
      });
    }
    if (Property) {
      Property = inflection.singularize(Property);
      delete Entity[inflection.pluralize(Property)];
      Entity[Property] = reducedObject;
    }

    async.series(tasksC, function (err) {
      callback(err, reducedObject);
    });
  }
}

function reduceFieldsAndExpandObject(Model, fields, data, callback) {
  if (!fields) {
    callback(null, data);
  } else {
    recursionReduce(Model, data, null, fields, callback);
  }
};

/**
 * @param datasource the datasource that has all models in the app.
 */
module.exports = function(datasource) {
  dataSource = datasource;

  return {
    iterationParse: iterationParse,
    reduceFieldsAndExpandObject: reduceFieldsAndExpandObject
  };
}

