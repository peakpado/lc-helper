/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Helper methods and objects for testing.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


exports.datasource = function(config) {
  return require('./models')(config);
};

exports.appConfig = {
  app: {
    pgURL: 'postgres://postgres@localhost:5432/travis_ci_test2'
  }
};

exports.controllerOptions = {
  filtering: true,
  deletionRestrictions: {
    where: {
      status: 'DRAFT'
    }
  },
  queryConfig: {
    pageSize: 100
  }
};

exports.tcUser = {
  id: 20015014,
  name: 'Neil Hastings',
  handle: 'indy_qa3',
  picture: '//www.gravatar.com/avatar/b56ee7ec472676dfda65dd62bde2ff3e',
  perms: {
    challengeApp: true
  }
};

exports.challengeData = {
  title: 'Serenity Challenge',
  status: 'SUBMISSION',
  prizes: [500.00, 250.00],
  regStartAt: '2014-10-09T10:00:00-05:00',
  projectId: 'PROJECT1',
  projectSource: 'TOPCODER'
}