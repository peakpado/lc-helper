/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test paramHelper.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';

var assert = require('assert');
var errors = require('common-errors');
var paramHelper = require('../lib/paramHelper');


describe('paramHelper', function () {

  describe('parseLimitOffset method', function () {

    it('should parse limit param into filters object', function (done) {
      var filters = {};
      paramHelper.parseLimitOffset(filters, 'limit', '10');
      assert.equal(filters.limit, 10);
      done();
    });

    it('should throw validation error with null value', function (done) {
      var filters = {};
      assert.throws(function () {
          paramHelper.parseLimitOffset(filters, 'limit', null);
        },
        errors.ValidationError
      );
      done();
    });

    it('should throw validation error with array value', function (done) {
      var filters = {};
      assert.throws(function () {
          paramHelper.parseLimitOffset(filters, 'limit', [1, 2]);
        },
        errors.ValidationError
      );
      done();
    });

    it('should throw validation error with NaN value', function (done) {
      var filters = {};
      function Foo() {};
      assert.throws(function () {
          paramHelper.parseLimitOffset(filters, 'limit', '123abc');
        },
        errors.ValidationError
      );
      done();
    });
  });

  describe('parseOrderBy method', function () {
    var model = {
      rawAttributes: {
        'id': 'id field',
        'createdBy': 'createdBy field'
      }
    };
    var filters;
    beforeEach(function (done) {
      filters = {};
      done();
    });

    it('should parse orderBy param into filters object', function (done) {
      var filters = {};
      paramHelper.parseOrderBy(model, filters, 'createdBy desc nulls first');
      assert.equal(filters.order, '"createdBy" desc nulls first');
      done();
    });

    it('should throw validation error with null value', function (done) {
      var filters = {};
      assert.throws(function () {
          paramHelper.parseOrderBy(model, filters, null);
        },
        errors.ValidationError
      );
      done();
    });

    it('should throw validation error with array value', function (done) {
      var filters = {};
      assert.throws(function () {
          paramHelper.parseOrderBy(model, filters, [1, 2]);
        },
        errors.ValidationError
      );
      done();
    });

    it('should throw validation error with non asc or desc', function (done) {
      var filters = {};
      assert.throws(function () {
          paramHelper.parseOrderBy(model, filters, 'createdBy increasing');
        },
        errors.ValidationError
      );
      done();
    });
  });

  describe('parseFilter method', function () {
    var model = {
      rawAttributes: {
        'id': 'id field',
        'status': 'status field'
      }
    };
    var filters;
    beforeEach(function (done) {
      filters = { where: {} };
      done();
    });

    it('should parse filter = operator into filters object', function (done) {
      paramHelper.parseFilter(model, filters, 'status=DRAFT');
      assert.equal(filters.where.status, 'DRAFT');
      done();
    });

    it('should parse filter < operator into filters object', function (done) {
      paramHelper.parseFilter(model, filters, 'status<DRAFT');
      assert.deepEqual(filters.where.status, {lt: "DRAFT"});
      done();
    });

    it('should parse filter in operator into filters object', function (done) {
      paramHelper.parseFilter(model, filters, 'status=in(DRAFT)');
      assert.equal(filters.where.status, 'DRAFT');
      done();
    });

    it('should throw validation error with null value', function (done) {
      assert.throws(function () {
          paramHelper.parseFilter(model, filters, null);
        },
        errors.ValidationError
      );
      done();
    });

    it('should throw validation error with array value', function (done) {
      assert.throws(function () {
          paramHelper.parseFilter(model, filters, [1, 2]);
        },
        errors.ValidationError
      );
      done();
    });

    it('should throw validation error with non asc or desc', function (done) {
      assert.throws(function () {
          paramHelper.parseFilter(model, filters, 'createdBy increasing');
        },
        errors.ValidationError
      );
      done();
    });


  });

});