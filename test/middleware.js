/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test middlewares.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var express = require('express');
var request = require('supertest');
var assert = require('assert');
var should = require('should');
var errors = require('common-errors');

var middleware = require('../').middleware;


describe('renderJson', function () {

  it('should render req.data as JSON', function (done) {
    var app = express();
    app.use(function (req, res) {
      req.data = {
        content: 'result'
      };
      middleware.renderJson(req, res);
    });

    request(app)
    .get('/')
    .expect(200, '{"content":"result"}', done);
  });

  it('should render empty response when no req.data', function (done) {
    var app = express();
    app.use(function (req, res) {
      middleware.renderJson(req, res);
    });

    request(app)
    .get('/')
    .expect(200, '', done);
  });

});

describe('errorHandler', function () {

  it('should handle a127 validation error', function (done) {
    var app = express();
    app.use(function (req, res) {
      var err = {
        code: 'CODE-1',
        failedValidation: true,
        message: 'validation error'
      };
      middleware.errorHandler(err, req, res);
    });

    request(app)
    .get('/')
    .expect(400)
    .end(function (err, res) {
      res.body.result.success.should.be.false;
      res.body.result.status.should.equal(400);
      res.body.content.should.equal('validation error');
      done();
    });
  });

  it('should handle common-errors validation error', function (done) {
    var app = express();
    app.use(function (req, res) {
      var err = new errors.ValidationError();
      err.addError(new errors.ValidationError('error-1'));
      err.addError(new errors.ValidationError('error-2'));
      middleware.errorHandler(err, req, res);
    });

    request(app)
    .get('/')
    .expect(400)
    .end(function (err, res) {
      res.body.result.success.should.be.false;
      res.body.result.status.should.equal(400);
      res.body.content.should.equal('error-1. error-2');
      done();
    });
  });

  it('should handle common-errors NotFoundError', function (done) {
    var app = express();
    app.use(function (req, res) {
      var err = new errors.NotFoundError('entity-1');
      middleware.errorHandler(err, req, res);
    });

    request(app)
    .get('/')
    .expect(404)
    .end(function (err, res) {
      res.body.result.success.should.be.false;
      res.body.result.status.should.equal(404);
      res.body.content.should.equal('Not Found: "entity-1"');
      done();
    });
  });

  it('should handle common-errors AuthenticationRequiredError', function (done) {
    var app = express();
    app.use(function (req, res) {
      var err = new errors.AuthenticationRequiredError('user-1');
      middleware.errorHandler(err, req, res);
    });

    request(app)
    .get('/')
    .expect(401)
    .end(function (err, res) {
      res.body.result.success.should.be.false;
      res.body.result.status.should.equal(401);
      res.body.content.should.equal('An attempt was made to perform an operation without authentication: user-1');
      done();
    });
  });


});