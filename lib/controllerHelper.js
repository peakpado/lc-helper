/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Helper methods for controller logic.
 *
 * @version 1.0
 * @author peakpado
 */
'use strict';

var async = require('async');
var _ = require('lodash');
var errors = require('common-errors');
var tcAuth = require('tc-auth');
var routeHelper = require('./routeHelper');
var paramHelper = require('./paramHelper');

var partialResponseHelper;

/**
 * Find an entity with the provided filters and its own id.
 * @param model the entity model
 * @param filters the current filters
 * @param req the request
 * @param callback the async callback
 */
function _findEntityByFilter(model, filters, req, callback) {
  // add the id parameter to the filters
  var idParam = routeHelper.getRefIdField(model);
  var idParamValue = req.swagger.params[idParam].value;
  var idValue = Number(idParamValue);
  // check id is valid numer and positive number
  if (_.isNaN(idValue) || idValue < 0) {
    callback(new errors.ValidationError('Invalid id parameter '+idParamValue));
  } else {
    var refFilters = _.cloneDeep(filters);
    refFilters.where.id = idValue;
    model.find(refFilters).success(function (entity) {
      if (!entity) {
        callback(new errors.NotFoundError(model.name + ' with id '+idParamValue));
      } else {
        callback(null, filters, entity);
      }
    })
    .error(function (err) {
      callback(new errors.Error('DBReadError: '+err.message, err));
    });
  }
}

function _checkIdParamAndRequestBody(referenceModels, req, callback) {
  if(!referenceModels) {
    return callback();
  }
  referenceModels.forEach(function (refModel) {
    var idParam = routeHelper.getRefIdField(refModel);
    var idParamValue = req.swagger.params[idParam].value;
    var idValue = Number(idParamValue);
    if(req.body[idParam] && idValue!==Number(req.body[idParam])) {
      return callback(new errors.ValidationError(idParam + ' value should be same in path param as well as request body'));
    }
  });
  callback();
}

/**
 * Build filters from request query parameters.
 * @param model the entity model
 * @param options the options related the query
 * @param filters the current filters
 * @param req the request
 * @param callback the async callback
 */
function _buildQueryFilter(model, options, filters, req, callback) {
  if (!filters) {
    filters = { where: {} };   // start with emtpty filter
  }

  var err;
  if (options.filtering) {
    filters.offset = 0;
    filters.limit = options.queryConfig.pageSize;
    // req.swagger.params returns empty value for non-existing parameters, it can't determine it's non-existing
    // or empty value. So req.query should be used to validate empty value and not-supportd parameters.
    // parse request parameters.
    try {
      _.each(_.keys(req.query), function (key) {
        if (key === 'offset' || key === 'limit') {
          paramHelper.parseLimitOffset(filters, key, req.query[key]);
        } else if (key === 'orderBy') {
          paramHelper.parseOrderBy(model, filters, req.query[key]);
        } else if (key === 'filter') {
          paramHelper.parseFilter(model, filters, req.query[key]);
        } else {
          throw new errors.ValidationError('The request parameter ' + key + ' is not supported');
        }
      });
    } catch (validationError) {
      if (err) {
        err.addError(err);
      } else {
        err = validationError;
      }
    }
  }
  callback(err, filters);
}

/**
 * Build filters from reference models.
 * @param referenceModels the array of referencing models
 * @param req the request
 * @param callback the async callback
 */
function _buildReferenceFilter(referenceModels, req, callback) {
  var filters = { where: {} };   // start with emtpty filter
  if (!referenceModels) {
    callback(null, filters);
  } else {
    async.eachSeries(referenceModels, function (refModel, cb) {
      var idParam = routeHelper.getRefIdField(refModel);
      var idParamValue = req.swagger.params[idParam].value;
      var idValue = Number(idParamValue);
      // check id is valid numer and positive number
      if (_.isNaN(idValue) || idValue < 0) {
        cb(new errors.ValidationError('Invalid id parameter '+idParamValue));
      } else {
        var refFilters = _.cloneDeep(filters);
        refFilters.where.id = idValue;
        // verify an element exists in the reference model
        refModel.find(refFilters).success(function (refEntity) {
          if(!refEntity) {
            cb(new errors.ValidationError('Cannot find the '+ refModel.name + ' with id '+idParamValue));
          } else {
            // add the id of reference element to filters
            filters.where[idParam] = refEntity.id;
            cb(null);
          }
        }).error(function (err) {
            cb(new errors.Error('DBReadError: '+err.message, err));
        });
      }
    }, function (err) {
      // pass err and filters to the next function in async
      callback(err, filters);
    });
  }
}

/**
 * Return error if there are extra parameters.
 * @param req the request
 * @param callback the async callback
 */
function _checkExtraParameters(req, callback) {
  if (_.keys(req.query).length > 0) {
    callback(new errors.ValidationError('Query parameter is not allowed'));
  } else {
    callback(null);
  }
}

/**
 * Parse fields parameter if exist in all get call.
 * @param req the request
 * @param callback the callback
 */
function _parsePartialResponseFields(req, callback) {
  var param = req.query.fields;
  delete req.query.fields;
  if (param && (typeof param === 'string')) {
    param = param.trim();
    var fields = {};

    try {
      partialResponseHelper.iterationParse(param, fields);
    } catch (err) {
      return callback(err);
    }
    // save to req object
    req.parseResponseFields = fields;
  }
  callback();
};

/**
 * This function retrieves all entities in the model filtered by referencing model
    and search criterias if filtering is enabled.
 * @param model the entity model
 * @param referenceModels the array of referencing models
 * @param options the controller options
 * @param req the request
 * @param res the response
 * @param next the next function in the chain
 */
function getAllEntities(model, referenceModels, options, req, res, next) {
  async.waterfall([
    function (callback) {   // parse partial response fields
      _parsePartialResponseFields(req, callback);
    },
    function (callback) {
      if (!options.filtering) {
        _checkExtraParameters(req, callback);
      } else {
        callback(null);
      }
    },
    function (callback) {
      _buildReferenceFilter(referenceModels, req, callback);
    },
    function (filters, callback) {
      _buildQueryFilter(model, options, filters, req, callback);
    },
    function (filters, callback) {
      // use entity filter IDs if configured
      if (options.entityFilterIDs) {
        filters.where = _.omit(filters.where, function (value, key) {
          return options.entityFilterIDs.indexOf(key) === -1;
        });
      }
      // add custom filters
      if(options.customFilters) {
        _.merge(filters, options.customFilters);
      }

      model.findAndCountAll(filters).success(function (result) {
        callback(null, result.count, result.rows);
      })
      .error(function (err) {
        callback(new errors.Error('DBReadError: '+err.message, err));
      });
    },
    function(totolCount, entities, callback) {  // handle partial response
      partialResponseHelper.reduceFieldsAndExpandObject(model, req.parseResponseFields, entities, function (err, reducedObject) {
        if (err) {
          callback(err);
        } else {
          callback(null, totolCount, reducedObject);
        }
      });
    }
  ], function (err, totalCount, entities) {
    if (err) {
      return next(err);  // go to error handler
    } else {
      req.data = {
        success: true,
        status: 200,
        metadata: {
          totalCount: totalCount
        },
        content: entities
      };
    }
    next();
  });

}

/**
 * This function gets an entity by id.
 * @param model the entity model
 * @param referenceModels the array of referencing models
 * @param options the controller options
 * @param res the response
 * @param next the next function in the chain
 */
function getEntity(model, referenceModels, options, req, res, next) {
  async.waterfall([
    function (callback) {   // parse partial response fields
      _parsePartialResponseFields(req, callback);
    },
    function (callback) {
      _checkExtraParameters(req, callback);
    },
    function (callback) {
      _buildReferenceFilter(referenceModels, req, callback);
    },
    function (filters, callback) {
      // use entity filter IDs if configured
      if (options.entityFilterIDs) {
        filters.where = _.omit(filters.where, function (value, key) {
          return options.entityFilterIDs.indexOf(key) === -1;
        });
      }
      _findEntityByFilter(model, filters, req, callback);
    },
    function (filters, entity, callback) {  // handle partial response
      partialResponseHelper.reduceFieldsAndExpandObject(model, req.parseResponseFields, entity, function (err, reducedObject) {
        if (err) {
          callback(err);
        } else {
          callback(null, reducedObject);
        }
      });
    }
  ], function (err, entity) {
    if (err) {
      return next(err);  // go to error handler
    } else {
      req.data = {
        success: true,
        status: 200,
        content: entity
      };
    }
    next();
  });

}

/**
 * This function creates an entity.
 * @param model the entity model
 * @param referenceModels the array of referencing models
 * @param options the controller options
 * @param req the request
 * @param res the response
 * @param next the next function in the chain
 */
function createEntity(model, referenceModels, options, req, res, next) {
  async.waterfall([
    function (callback) {
      _checkIdParamAndRequestBody(referenceModels, req, callback);
    },
    function (callback) {
      _checkExtraParameters(req, callback);
    },
    function (callback) {
      _buildReferenceFilter(referenceModels, req, callback);
    },
    function (filters, callback) {
      // exclude prohibited fields
      var data = _.omit(req.swagger.params.body.value, 'id', 'createdAt', 'updatedAt', 'createdBy', 'updatedBy');
      // set createdBy and updatedBy user
      data.createdBy = tcAuth.getSigninUser(req).id;
      data.updatedBy = tcAuth.getSigninUser(req).id;
      // add foreign keys
      _.extend(data, filters.where);
      model.create(data).success(function (entity) {
        callback(null, entity);
      })
      .error(function (err) {
        callback(new errors.Error('DBCreateError: '+err.message, err));
      });
    }
  ], function (err, entity) {
    if (err) {
      return next(err);   // go to error handler
    } else {
      req.data = {
        id: entity.id,
        result: {
          success: true,
          status: 200
        }
      };
    }
    next();
  });
}

/**
 * This function updates an entity.
 * @param model the entity model
 * @param referenceModels the array of referencing models
 * @param options the controller options
 * @param req the request
 * @param res the response
 * @param next the next function in the chain
 */
function updateEntity(model, referenceModels, options, req, res, next) {
  async.waterfall([
    function (callback) {
      _checkIdParamAndRequestBody(referenceModels, req, callback);
    },
    function (callback) {
      _checkExtraParameters(req, callback);
    },
    function (callback) {
      _buildReferenceFilter(referenceModels, req, callback);
    },
    function (filters, callback) {
      // use entity filter IDs if configured
      if (options.entityFilterIDs) {
        filters.where = _.omit(filters.where, function(value, key) {
          return options.entityFilterIDs.indexOf(key) === -1;
        });
      }
      _findEntityByFilter(model, filters, req, callback);
    },
    function (filters, entity, callback) {
      var excludeFields = Object.keys(filters.where);
      _.map(['id', 'createdAt', 'updatedAt', 'createdBy', 'updatedBy'], function (field) {
        excludeFields.push(field);
      });
      // exclude prohibited fields
      var data = _.omit(req.swagger.params.body.value, excludeFields);
      _.extend(entity, data);
      entity.updatedBy = tcAuth.getSigninUser(req).id;
      entity.save().success(function () {
        callback(null, entity);
      })
      .error(function (err) {
        callback(new errors.Error('DBSaveError: '+err.message, err));
      });
    }
  ], function (err, entity) {
    if (err) {
      return next(err);   // go to error handler
    } else {
      req.data = {
        id: entity.id,
        result: {
          success: true,
          status: 200
        }
      };
    }
    next();
  });

}

/**
 * This function deletes an entity.
 * @param model the entity model
 * @param referenceModels the array of referencing models
 * @param options the controller options
 * @param req the request
 * @param res the response
 * @param next the next function in the chain
 */
function deleteEntity(model, referenceModels, options, req, res, next) {
  async.waterfall([
    function (callback) {
      _checkExtraParameters(req, callback);
    },
    function (callback) {
      _buildReferenceFilter(referenceModels, req, callback);
    },
    function (filters, callback) {
      // use entity filter IDs if configured
      if (options.entityFilterIDs) {
        filters.where = _.omit(filters.where, function (value, key) {
          return options.entityFilterIDs.indexOf(key) === -1;
        });
      }

      // add custom restriction to filters
      if(options.deletionRestrictions) {
        _.merge(filters, options.deletionRestrictions);
      }

      _findEntityByFilter(model, filters, req, callback);
    },
    function (filters, entity, callback) {
      entity.destroy().success(function () {
        callback(null, entity);
      })
      .error(function (err) {
        callback(new errors.Error('DBDeleteError: '+err.message, err));
      });
    }
  ], function (err, entity) {
    if (err) {
      return next(err);   // go to error handler
    } else {
      req.data = {
        id: entity.id,
        result: {
          success: true,
          status: 200
        }
      };
    }
    next();
  });

}

/**
 * Build the CRUD controller for a model.
 * @param model the model to build a controller for
 * @param referenceModels the list of models that reference current model
 * @param options the options for building controller
 */
function buildController(model, referenceModels, options) {
  var controller = {};

  // Get an entity.
  controller.get = function(req, res, next) {
    getEntity(model, referenceModels, options, req, res, next);
  };

  // Create an entity.
  controller.create = function(req, res, next) {
    createEntity(model, referenceModels, options, req, res, next);
  };

  // Update an entity.
  controller.update = function(req, res, next) {
    updateEntity(model, referenceModels, options, req, res, next);
  };

  // Retrieve all entities.
  controller.all = function(req, res, next) {
    getAllEntities(model, referenceModels, options, req, res, next);
  };

  // Delete an entity.
  controller.delete = function(req, res, next) {
    deleteEntity(model, referenceModels, options, req, res, next);
  };

  return controller;
};


/**
 * @param datasource the datasource that has all models in the app.
 */
module.exports = function(datasource) {
  partialResponseHelper = require('./partialResponseHelper')(datasource);

  return {
    buildController: buildController
  };
};
