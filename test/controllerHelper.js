/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test controllerHelper.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';

var async = require('async');
var assert = require('assert');
var errors = require('common-errors');

var testHelper = require('./test-helper');


var datasource = require('./models')(testHelper.appConfig);
var Challenge = datasource.Challenge;
var Participant = datasource.Participant;


var controllerHelper = require('../').controllerHelper(datasource);

// build controller for challenge resource
var challengeController = controllerHelper.buildController(Challenge, null, testHelper.controllerOptions);

describe('controllerHelper', function () {

  var challenges = [];
  before(function(done) {
    var statuses = ['DRAFT', 'SUBMISSION', 'REVIEW', 'COMPLETE'];
    // create 5 challenges
    async.timesSeries(5, function(index, callback) {
      // create a challenge
      var challengeData = {
        title: 'Serenity Challenge '+index,
        status: statuses[index % 3],
        regStartAt: '2014-10-0'+(index+1),  // index starts at 0
        projectId: 'PROJECT1',
        projectSource: 'TOPCODER',
        createdBy: index
      };
      Challenge.create(challengeData).success(function(savedEntity) {
        challenges.push(savedEntity);
        callback();
      });
    }, function(err) {
      done();
    });
  });

  it('should get all entities', function (done) {
    var req = {
      query: {}
    };
    var res = {};
    challengeController.all(req, res, function (err, req2, res2, next2) {
      // verify req.data
      assert(!err);
      assert(req.data.success);
      assert(req.data.status, 200);
      assert(req.data.content.length >= 5);
      done();
    });
  });

  it('should get an entity by id', function (done) {
    var challenge = challenges[0];
    var req = {
      query: {},
      swagger: {
        params: {
          challengeId: {
            value: challenge.id
          }
        }
      }
    };
    var res = {};
    challengeController.get(req, res, function (err, req2, res2, next2) {
      // verify req.data
      assert(!err);
      assert(req.data.success);
      assert(req.data.status, 200);
      assert.equal(req.data.content.title, challenge.title);
      done();
    });
  });

  it('should create an entity', function (done) {
    var req = {
      query: {},
      tcUser: testHelper.tcUser,
      swagger: {
        params: {
          body: {
            value: testHelper.challengeData
          }
        }
      }
    };
    var res = {};
    challengeController.create(req, res, function (err, req2, res2, next2) {
      // verify req.data
      assert(!err);
      assert(req.data.id);
      assert(req.data.result.success);
      assert.equal(req.data.result.status, 200);
      done();
    });
  });

  it('should update an entity', function (done) {
    var challenge = challenges[0];
    challenge.title = 'new title';
    var req = {
      query: {},
      tcUser: testHelper.tcUser,
      swagger: {
        params: {
          challengeId: {
            value: challenge.id
          },
          body: {
            value: challenge
          }
        }
      }
    };
    var res = {};
    challengeController.update(req, res, function (err, req2, res2, next2) {
      // verify req.data
      assert(!err);
      assert.equal(req.data.id, challenge.id);
      assert(req.data.result.success);
      assert.equal(req.data.result.status, 200);
      done();
    });
  });

  it('should delete an entity', function (done) {
    var challenge = challenges[0];
    var req = {
      query: {},
      tcUser: testHelper.tcUser,
      swagger: {
        params: {
          challengeId: {
            value: challenge.id
          }
        }
      }
    };
    var res = {};
    challengeController.delete(req, res, function (err, req2, res2, next2) {
      // verify req.data
      assert(!err);
      assert.equal(req.data.id, challenge.id);
      assert(req.data.result.success);
      assert.equal(req.data.result.status, 200);
      done();
    });

  });

  it('should fail to delete an entity that has delete-restriction', function (done) {
    var challenge = challenges[1];  // non-DRAFT can't be deleted
    var req = {
      query: {},
      tcUser: testHelper.tcUser,
      swagger: {
        params: {
          challengeId: {
            value: challenge.id
          }
        }
      }
    };
    var res = {};
    challengeController.delete(req, res, function (err, req2, res2, next2) {
      // verify req.data
      assert(err);
      done();
    });

  });

  after(function(done) {
    // delete data created during test.
    async.eachSeries([Participant, Challenge], function(model, callback) {
      model.findAll().success(function(entities) {
        async.each(entities, function(entity, cb) {
          entity.destroy().success(function() {
            cb();
          })
          .error(function(err) {
            cb();
          });
        }, function(err) {
          callback();
        });
      });
    }, function(err) {
      done();
    });
  });

});