lc-helper
===========

Common helper functions and middlewares for lc projects

* Controller helper to build CRUD controllers
* Partial response helper to implement a [Google partial response](https://developers.google.com/+/api/#partial-responses)
* middlewares for error handler and rendering json


## Installation
```npm install lc-helper```

## Usage

### Controler Helper

The controllerHelp requires datasource which is a collection of models used in the application.
	
	var controllerHelper = lcHelper.controllerHelper(datasource);
	var challengeController = controllerHelper.buildController(Challenge, null, options);

The buildController() method creates all CRUD routes for Challenge model.

### Route Helper

The routeHelper exports two utility functions.


* createError(message, statuscode) creates a Error object with message and statusCode.
* getRefIdField(referenceModel) returns a foreign key of reference model.


### Middlewares

* renderJson render the result data in req.data as JSON
* errorHandler handle error in the application and send an error message to client as JSON.


## Test
To run the test, type `npm test`.

	$ npm test
	
## Authors

This library was developed by [peakpado](peakpado@gmail.com) at [TopCoder](http://www.topcoder.com)


## License

Copyright (c) 2014 TopCoder, Inc. All rights reserved.